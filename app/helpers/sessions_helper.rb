module SessionsHelper
  
 # Logs in the given volunteer.
  def log_in(volunteer)
    session[:volunteer_id] = volunteer.id
  end
  
  # Remembers a volunteer in a persistent session.
  def remember(volunteer)
    volunteer.remember
    cookies.permanent.signed[:volunteer_id] = volunteer.id
    cookies.permanent[:remember_token] = volunteer.remember_token
  end
  
  # Returns true if the given volunteer is the current volunteer.
  def current_volunteer?(volunteer)
    volunteer == current_volunteer
  end
  
  # Returns the current logged-in volunteer (if any).
  def current_volunteer
    if (volunteer_id = session[:volunteer_id])
      @current_volunteer ||= Volunteer.find_by(id: volunteer_id)
    elsif (volunteer_id = cookies.signed[:volunteer_id])
      volunteer = Volunteer.find_by(id: volunteer_id)
      if volunteer && volunteer.authenticated?(:remember, cookies[:remember_token])
        log_in volunteer
        @current_volunteer = volunteer
      end
    end
  end
  
  
  # Returns true if the volunteer is logged in, false otherwise.
  def logged_in?
    !current_volunteer.nil?
  end
  
  # Forgets a persistent session.
  def forget(volunteer)
    volunteer.forget
    cookies.delete(:volunteer_id)
    cookies.delete(:remember_token)
  end

  # Logs out the current volunteer.
  def log_out
    forget(current_volunteer)
    session.delete(:volunteer_id)
    @current_volunteer = nil
  end
  
  # Redirects to stored location (or to the default).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
  
end