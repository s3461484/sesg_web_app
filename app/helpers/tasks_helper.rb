module TasksHelper
  def check_task
    t = Task.find_by(:id => params[:id])
    if t.nil?
      flash[:danger] = "No task with that ID found"
      redirect_to events_path
    end
  end
end
