module VolunteersHelper

  # Returns the Gravatar for the given volunteer.
  def check_volunteer
    v = Volunteer.find_by(:id => params[:id])
    if v.nil?
      flash[:danger] = "No volunteer with that ID found"
      redirect_to volunteers_path
    end
  end
end