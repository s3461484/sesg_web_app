module EventsHelper
  def check_event
    v = Event.find_by(:id => params[:id])
    if v.nil?
      flash[:danger] = "No event with that ID found"
      redirect_to events_path
    end
  end
end
