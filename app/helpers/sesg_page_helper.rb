module SesgPageHelper
  
  def in_event(v, e)
    e.tasks.each do |t|
      if t.volunteers.include?(v)
        return true
      end 
    end 
    return false
  end
  
  def in_task(v, t)
    return t.volunteers.include?(v)
  end
end
