module ApplicationHelper
  def full_title(page_title = '')
    base_title = "SESG"
    if page_title.empty?
      base_title.html_safe
    else
      "#{page_title} | #{base_title}".html_safe
    end
  end
  
  def tag_name(t)
    if t == "awn"
      return "Absent With Notice"
    elsif t == "awon"
      return "Absent Without Notice"
    elsif t == "lwn"
      return "Late With Notice"
    elsif t == "lwon"
      return "Late Without Notice"
    elsif t == "ontime"
      return "On Time"
    elsif t == "bonus"
      return "Bonus"
    else
      return "Minus"
    end
  end 
  
end
