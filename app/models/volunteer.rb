class Volunteer < ActiveRecord::Base
  has_many :registers, dependent: :destroy
  has_many :tasks, through: :registers
  
  attr_accessor :remember_token, :reset_token
  before_save { self.email = email.downcase }
  
  validates :name,  presence: true, length: { maximum: 50 }
  validates :points, :numericality => {:only_integer => true}
  validates :phone, :numericality => {:only_integer => true}
  validates :phone,  presence: true, length: { maximum: 12, minimum: 9 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@rmit.edu.vn\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, length: { minimum: 6 }, allow_blank: true
  default_scope -> { order(created_at: :asc) }
  
   # Returns the hash digest of the given string.
  def Volunteer.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def Volunteer.new_token
    SecureRandom.urlsafe_base64
  end
  
  def create_reset_digest
    self.reset_token = Volunteer.new_token
    update_attribute(:reset_digest,  Volunteer.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end
  
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end
  
  # Sends password reset email.
  def send_password_reset_email
    VolunteerMailer.password_reset(self).deliver_now
  end
  
  # Remembers a volunteer in the database for use in persistent sessions.
  def remember
    self.remember_token = Volunteer.new_token
    update_attribute(:remember_digest, Volunteer.digest(remember_token))
  end
  
  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    abc = send("#{attribute}_digest")
    return false if abc.nil?
    BCrypt::Password.new(abc).is_password?(token)
  end
  
  # Forgets a volunteer.
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  # Enable filterrific to search
  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc', core: 'all' },
    available_filters: [
      :search_query,
      :sorted_by,
      :core,
      :with_points_gte,
      :with_points_lt,
      :with_created_at_gte,
      :with_tags,
      :with_created_at_lt,
      :with_any_task_ids,
      :with_programs,
    ]
  )
  
  # Define scopes
  scope :search_query, lambda { |query|
    return nil  if query.blank?
    terms = query.downcase.split(/\s+/)
    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      ('%' + e.gsub('*', '%') + '%').gsub(/%+/, '%')
    }
    
    num_or_conditions = 3
    where(
      terms.map {
        or_clauses = [
          "LOWER(volunteers.name) LIKE ?",
          "LOWER(volunteers.phone) LIKE ?",
          "LOWER(volunteers.email) LIKE ?"
        ].join(' OR ')
        "(#{ or_clauses })"
      }.join(' AND '),
      *terms.map { |e| [e] * num_or_conditions }.flatten
    )
  }
  
  scope :sorted_by, lambda { |sort_option|
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at_/
      order("volunteers.created_at #{ direction }")
    when /^name_/
      order("LOWER(volunteers.name) #{ direction }")
    else
      raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
    end
  }
  
  scope :core, lambda { |core_option|
    #core_option = core_option ? 'volunteer' : 'core'
    case core_option
    when 'all'
    
    when 'volunteer'
      where('volunteers.core = ?', false)
    when 'core'
      where('volunteers.core = ?', true)
    else
      
    end
  }
  
  
  scope :with_any_task_ids, lambda{ |task_ids|
    # return nil  if task_name.blank?
    # task_ids = where([
    #   %(
    #     EXISTS (
    #       SELECT 1
    #         FROM registers, tasks
    #       WHERE tasks.id = registers.task_id
    #         AND tasks.name = ?)
    #   ),
    #   task_name
    # ]).flatten
    # get a reference to the join table
    registers = Register.arel_table
    # get a reference to the filtered table
    volunteers = Volunteer.arel_table
    # let AREL generate a complex SQL query
    where(
      Register \
        .where(registers[:volunteer_id].eq(volunteers[:id])) \
        .where(registers[:task_id].in([*task_ids].map(&:to_i))) \
        .exists
    )
  }
  
  scope :with_created_at_gte, lambda { |ref_date|
    where('volunteers.created_at >= ?', ref_date)
  }
  
  scope :with_created_at_lt, lambda { |ref_date|
    where('volunteers.created_at < ?', ref_date)
  }
  
  scope :with_points_gte, lambda { |point|
    where('volunteers.points >= ?', point)
  }
  
  scope :with_points_lt, lambda { |point|
    where('volunteers.points < ?', point)
  }
  
  scope :with_tags, lambda { |tag|
    where([
      %(
        EXISTS (
          SELECT 1
            FROM volunteers, registers
           WHERE volunteers.id = registers.volunteer_id
             AND registers.tag = ?)
      ),
      tag
    ])
  }
  
  scope :with_programs, lambda { |program|
    where('volunteers.program = ?', program)
  }
  
  def self.options_for_programs 
    [
      ["Bachelor of Bussiness (Acountancy)","Bachelor of Bussiness (Acountancy)"],
      ["Bachelor of Bussiness (BIS)","Bachelor of Bussiness (BIS)"],
      ["Bachelor of Commerce","Bachelor of Commerce"],
      ["Bachelor of Business (Economics & Finance)","Bachelor of Business (Economics & Finance)"],
      ["Bachelor of Business (Marketing)","Bachelor of Business (Marketing)"],
      ["Bachelor of Business (Entrepreneurship)","Bachelor of Business (Entrepreneurship)"],
      ["Bachelor of Business (Management)","Bachelor of Business (Management)"],
      ["Bachelor of Business (International Business)","Bachelor of Business (International Business)"],
      ["Bachelor of Communication (Professional Communication)","Bachelor of Communication (Professional Communication)"],
      ["Bachelor of Design (Digital Media)","Bachelor of Design (Digital Media)"],
      ["Diploma of Design (Digital Media)","Diploma of Design (Digital Media)"],
      ["Bachelor of Fashion (Merchandise Management)","Bachelor of Fashion (Merchandise Management)"],
      ["Bachelor of IT","Bachelor of IT"],
      ["Bachelor of Engineering (Electrical and Electronics)", "Bachelor of Engineering (Electrical and Electronics)"],
      ["English","English"],
      ["MBA","MBA"]
    ]
  end
  
  def self.options_for_tags
    [
      ['Absent with notice', 'awn'],
      ['Absent without notice', 'awon'],
      ['Late with notice', 'lwn'],
      ['Late without notice', 'lwon'],   
      ['Ontime', 'ontime'],
      ['Bonus', 'bonus'],
      ['Minus', 'minus'],
    ]
  end
  
  def self.options_for_sorted_by
    [
      ['Name (A-Z)', 'name_asc'],
      ['Newest Registration', 'created_at_desc'],
      ['Oldest Registration', 'created_at_asc'],
    ]
  end
  
  def self.options_for_core
    [
      ['All', 'all'],
      ['Core', 'core'],
      ['Volunteer', 'volunteer'],
    ]
  end

  def decorated_created_at
    created_at.to_date.to_s(:long)
  end
  
  private
    def create_activation_digest
      self.activation_token  = Volunteer.new_token
      self.activation_digest = Volunteer.digest(activation_token)
    end
  
end
