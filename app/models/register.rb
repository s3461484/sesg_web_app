class Register < ActiveRecord::Base
  belongs_to :volunteer
  belongs_to :task
  default_scope -> { order(created_at: :asc) }
  
  def send_reminder
    task = self.task
    vol = self.vol
    time = task.start_time - 2.days
    if time.to_date == Date.today
      VolunteerMailer.reminder(vol,task).deliver_now
    end  
  end
  
end
