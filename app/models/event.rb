class Event < ActiveRecord::Base
  has_many :tasks, dependent: :destroy
  validates :name,  presence: true, length: { maximum: 50 }
  default_scope -> { order(created_at: :desc) }
  validates :start_date, :end_date, presence: true
  validate :start_date_before_end_date
  
  def start_date_before_end_date
    if !start_date.nil? && !end_date.nil?
      if start_date > end_date
        errors.add(:start_date, "Must Occured Before End Date")
      end
    end
  end
  
  # Enable filterrific to search
  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc', current: 'all' },
    available_filters: [
      :search_query,
      :sorted_by,
      :current,
      :with_start_date_gte,
      :with_start_date_lt
    ]
  )
  
  # Define scopes
  scope :search_query, lambda { |query|
    return nil  if query.blank?
    terms = query.downcase.split(/\s+/)
    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      (e.gsub('*', '%') + '%').gsub(/%+/, '%')
    }
    num_or_conditions = 1
    where(
      terms.map {
        or_clauses = [
          "LOWER(events.name) LIKE ?",
        ].join(' OR ')
        "(#{ or_clauses })"
      }.join(' AND '),
      *terms.map { |e| [e] * num_or_conditions }.flatten
    )
  }
  
  scope :sorted_by, lambda { |sort_option|
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at_/
      order("events.created_at #{ direction }")
    when /^name_/
      order("LOWER(events.name) #{ direction }")
    else
      raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
    end
  }
  
  scope :current, lambda { |current_date|
    case current_date
    when 'all'
    
    when 'available'
      where(['events.start_date >= ? AND events.end_date >= ?', current_date, current_date])
      where(['events.start_date <= ? AND events.end_date >= ?', current_date, current_date])
    when 'past'
      where(['events.start_date <= ? AND events.end_date <= ?', current_date, current_date])
    else
      
    end
  }
  
  
  scope :with_start_date_gte, lambda { |ref_date|
    where('events.start_date >= ?', ref_date)
  }
  
  scope :with_start_date_lt, lambda { |ref_date|
    where('events.start_date < ?', ref_date)
  }
  
  def self.options_for_sorted_by
    [
      ['Name (A-Z)', 'name_asc'],
      ['Date Created (Newest First)', 'created_at_desc'],
      ['Date Created (Oldest First)', 'created_at_asc'],
    ]
  end
  
  def self.options_for_current
    [
      ['All', 'all'],
      ['Past', 'past'],
      ['Available', 'available'],
    ]
  end

  def decorated_start_date
    start_date.to_date.to_s(:long)
  end
  
end