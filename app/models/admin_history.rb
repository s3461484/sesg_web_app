class AdminHistory < ActiveRecord::Base
    default_scope -> { order(created_at: :desc) }
    before_save :limit_admin_history
  
  def limit_admin_history
    if AdminHistory.all.size >= 200
      AdminHistory.last.destroy
    end
  end
end
