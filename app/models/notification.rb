class Notification < ActiveRecord::Base
  default_scope -> { order(created_at: :desc) }
  before_save :limit_noti
  
  def limit_noti
    if Notification.all.size >= 2000
      Notification.last.destroy
    end
  end
end
