class Task < ActiveRecord::Base
  belongs_to :event
  has_many :registers, dependent: :destroy
  has_many :volunteers, through: :registers
  has_one :restriction, dependent: :destroy
  validates :name,  presence: true, length: { maximum: 50 }
  default_scope -> { order(created_at: :desc) }
  validates :start_time, :end_time, :awn, :awon, :lwn, :lwon, :ontime, :bonus, :minus, :max_volunteer_number, presence: true
  validate :start_time_before_end_time
  
  def start_time_before_end_time
    if !start_time.nil? && !end_time.nil?
      if start_time > end_time
        errors.add(:start_time, "Must Occured Before End Time")
      end
    end
  end

end