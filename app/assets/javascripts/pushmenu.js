var ready;
ready = function() {
  var showLeftPush = document.getElementById( 'showLeftPush' ),
	  menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
		body = document.body;


	showLeftPush.onclick = function() {
		classie.toggle( this, 'active' );
		classie.toggle( body, 'cbp-spmenu-push-toright' );
		classie.toggle( menuLeft, 'cbp-spmenu-open' );
		disableOther( 'showLeftPush' );
	};

	function disableOther( button ) {
		if( button != 'showLeftPush' ) {
			classie.toggle( showLeftPush, 'disabled' );
		}

	}
};
$(document).ready(ready);
$(document).on("page:load",ready);
