class TasksController < ApplicationController
  # Filters
  before_action :logged_in_volunteer
  before_action :is_admin_or_core,     only: [:create, :destroy, :edit, :update, :new]
  before_action :check_task, only: [:show, :edit, :update, :destroy]
  
  # Show
  def show
    @task = Task.find(params[:id])
    @register = Register.new
    @registers = @task.registers
    
    restricts = @task.restriction
    @isRestricted = true
    if !restricts.nil?
      if restricts.task_restrict == "" && restricts.tag == ""
        @isRestricted = false
      elsif !restricts.task_restrict == "" && !restricts.tag == ""
        regs = current_volunteer.registers
        regs.each do |re|
          if re.task_id == restricts.task_restrict && re.tag != restricts.tag
            @isRestricted = false
            break
          end
        end
      elsif restricts.task_restrict == ""
        regs = current_volunteer.registers
        @isRestricted = false
        regs.each do |re|
          if re.tag == restricts.tag
            @isRestricted = true
            break
          end
        end
      elsif restricts.tag == ""
        tasks = current_volunteer.tasks
        if tasks.include?(Task.find(restricts.task_restrict))
          @isRestricted = false
        end
      end
    else
      @isRestricted = false
    end
  end
  
  # Add
  def new
    @event = Event.find(params[:event_id])
    @task = Task.new
  end
  
  def create
    local_params = task_params.clone
    local_params[:current_volunteer_number] = 0
    local_params[:current_backup] = 0
    @task = Task.new(local_params)
    if @task.save
      flash[:success] = "Task Created!"
      if !(params[:task][:restrictions][:restriction_task] == nil && params[:task][:restrictions][:restriction_tag] == nil)
        Restriction.create(:task_id => @task.id.to_s, :tag => params[:task][:restrictions][:restriction_tag].to_s,
                                :task_restrict => params[:task][:restrictions][:restriction_task].to_s)
      end
      if current_volunteer.admin?
        AdminHistory.create(:content => "Created new task: #{@task.name}")
      else
        AdminHistory.create(:content => "#{current_volunteer.name} created new task: #{@task.name}")
      end
      redirect_to @task
    else
      flash[:danger] = "Some fields are missing"
      redirect_to "/tasks/new/#{task_params[:event_id]}"
    end
  end
  
  # Edit
  def edit
    @events = Event.all
    @task = Task.find(params[:id])
  end
  
  def update
    @events = Event.all
    @task = Task.find(params[:id])
    if @task.update_attributes(task_params)
      flash[:success] = "Task Updated!"
      vols = @task.volunteers
      vols_id = ""
      vols.each do |v|
        vols_id += v.id.to_s + ";"
      end
      evt = Event.find(@task[:event_id])
      if current_volunteer.admin?
        AdminHistory.create(:content => "Updated a task: #{evt.name} #{@task.name}")
      else
        AdminHistory.create(:content => "#{current_volunteer.name} updated a task: #{evt.name} #{@task.name}")
      end
      Notification.create(:sender_id => "SESG", :receiver_id => vols_id,
                          :event_id => @task.event_id.to_s,
                          :content => "Task #{@task.name} have been updated.")
      redirect_to @task
    else
      render 'edit'
    end
  end
  
  # Delete
  def destroy
    task = Task.find(params[:id])
    vols = task.volunteers
    vols_id = ""
    vols.each do |v|
      vols_id += v.id.to_s + ";"
    end
    evt = Event.find(task[:event_id])
    if current_volunteer.admin?
      AdminHistory.create(:content => "Deleted a task: #{evt.name} #{task.name}")
    else
      AdminHistory.create(:content => "#{current_volunteer.name} deleted a task: #{evt.name} #{task.name}")
    end
    Notification.create(:sender_id => "SESG", :receiver_id => vols_id,
                        :event_id => task.event_id.to_s,
                        :content => "Task #{task.name} have been cancelled.")
    task.destroy
    flash[:success] = "Task Deleted!"
    redirect_to events_path
  end
  
  private

    def task_params
      params.require(:task).permit(:name, :description, :start_time, :end_time,
                                   :awn, :awon,:lwn, :lwon, :ontime, :bonus, 
                                   :minus, :event_id, :task_type, 
                                   :max_volunteer_number, :max_backup)
    end
end