class EventsController < ApplicationController
  # Filters
  before_action :logged_in_volunteer, only: [:index]
  before_action :admin_volunteer,     only: [:create, :destroy, :edit, :update, :new]
  before_action :check_event, only: [:show, :edit, :update, :destroy]
  
  # Show
  def index
    @filterrific = initialize_filterrific(
      Event,
      params[:filterrific],
      :select_options => {
        sorted_by: Event.options_for_sorted_by,
        current: Event.options_for_current,
      }
    ) or return
    @events = @filterrific.find.page(params[:page])

    respond_to do |format|
      format.html
      format.js
    end
    
    # @task = Task.new
    # @events = Event.paginate(page: params[:page])
  end
  
  def show
    @event = Event.find(params[:id])
    @max = 0
    @current = 0
    if @event.tasks.any?
      @event.tasks.each do |t| 
        @max += t.max_volunteer_number + t.max_backup
        @current += t.current_volunteer_number + t.current_backup
      end
    end
  end
  
  # Add
  def new
    @event = Event.new
  end
  
  def create
    local_params = event_params.clone
    local_params["start_date(4i)"] = "0"
    local_params["end_date(4i)"] = "23"
    local_params["start_date(5i)"] = "0"
    local_params["end_date(5i)"] = "59"
    @event = Event.new(local_params)
    if @event.save
      flash[:success] = "Event Created!"
      AdminHistory.create(:content => "Created a new event: #{@event.name}, ID: #{@event.id}")
      Notification.create(:sender_id => "SESG", :receiver_id => "all",
                            :event_id => @event.id.to_s,
                            :content => "Event #{@event.name} is recruiting.")
      
      redirect_to @event
    else
      render 'new'
    end
  end
  # Edit
  def edit
    @event = Event.find(params[:id])
  end
  
  def update
    @event = Event.find(params[:id])
    if @event.update_attributes(event_params)
      flash[:success] = "Event Updated!"
      AdminHistory.create(:content => "Edited an event: #{@event.name}, ID: #{@event.id}")
      redirect_to @event
    else
      render 'edit'
    end
  end
    
  # Delete
  def destroy
    event = Event.find_by(:id => params[:id])
    AdminHistory.create(:content => "Deleted an event: #{event.name}, ID: #{event.id}")
    Notification.create(:sender_id => "SESG", :receiver_id => "all",
                        :event_id => event.id.to_s,
                        :content => "Event #{event.name} has been cancelled.")
    event.destroy
    flash[:success] = "Event Deleted!"
    redirect_to events_url
  end
  
  private

    def event_params
      params.require(:event).permit(:name, :description, :start_date,
                                   :end_date, :image_link)
    end
end