class SessionsController < ApplicationController
  def new
    if logged_in?
      redirect_to current_volunteer
    end
  end
  
  def create
    volunteer = Volunteer.find_by(email: params[:session][:email].downcase)
    if volunteer && volunteer.authenticate(params[:session][:password])
      log_in volunteer
      params[:session][:remember_me] == '1' ? remember(volunteer) : forget(volunteer)
      redirect_back_or volunteer
    else
      # Create an error message.
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end
  
  def change_locale
    session[:locale] = params[:lang]
    redirect_to(:back)
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
