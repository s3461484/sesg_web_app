class RegistersController < ApplicationController
  # Filters
  before_action :is_volunteer_or_core, only: [:create]
  before_action :logged_in_volunteer
  
  # Add
  def create
    @register = Register.new(register_params)
    @task = Task.find(register_params[:task_id])
    if (current_volunteer.core? && !@task.task_type?) || (!current_volunteer.core? && @task.task_type?)
      if @task.max_volunteer_number > @task.current_volunteer_number && !@task.volunteers.include?(current_volunteer)
        @register.save
        @task.update_attributes(current_volunteer_number: (@task.current_volunteer_number + 1).to_s)
        flash[:success] = "Register Successfully!"
        redirect_to events_path
      elsif @task.max_backup > @task.current_backup && !@task.volunteers.include?(current_volunteer)
        @register.save
        @task.update_attributes(current_backup: (@task.current_backup + 1).to_s)
        flash[:success] = "Registered to be Backup"
        redirect_to events_path
      else
        flash[:danger] = "Register Failed!"
        redirect_to events_path
      end
    else
      if current_volunteer.core?
        flash[:danger] = "Core member can only register for core tasks."
      else
        flash[:danger] = "Volunteer can only register for volunteer tasks."
      end
      redirect_to events_path
    end
  end
  
  def update_all
    # Take the hashes of :key => :register
    local_hash = params[:register][:reg]
    local_hash.each do |i, reg|
      @register = Register.find(reg[:register_id])
      if !reg[:tag].nil?
        ta1 = reg[:tag].to_sym
        @t = @register.task
        evt = Event.find(@t[:event_id])
        if @register.tag.nil?
          @register.update_attributes(:tag => ta1)
          volunteer = Volunteer.find(@register.volunteer)
          volunteer.update_attributes(:points => (volunteer.points + @t[ta1]).to_s )
        else
          ta2 = @register.tag.to_sym
          @register.update_attributes(:tag => ta1)
          volunteer = Volunteer.find(@register.volunteer)
          volunteer.update_attributes(:points => (volunteer.points + @t[ta1] - @t[ta2]).to_s )
        end
        AdminHistory.create(:content => "Update performance tag: #{evt.name} - 
              #{@t.name} - #{volunteer.name} - Points: #{volunteer.points} ")
      end
    end
    redirect_to @t
  end
  
  def destroy
    re = Register.find(params[:id])
    task = Task.find(re[:task_id])
    evt = Event.find(task[event_id])
    vol = Volunteer.find(re[volunteer_id])
    AdminHistory.create(:content => "Removed volunteer #{vol.name} from #{evt.name} - #{task.name}")
    Notification.create(:sender_id => "SESG", :receiver_id => re.id.to_s,
                        :event_id => task.event_id.to_s,
                        :content => "You have been removed from task #{task.name}")
    re.destroy
    if task.current_backup > 0
      regs = task.registers
      regs.each do |r|
        if r.backup?
          r.update_attributes(:backup => false)
          task.update_attributes(:current_backup => (task.current_backup - 1).to_s)
          Notification.create(:sender_id => "SESG", :receiver_id => r.volunteer_id.to_s,
                              :event_id => task.event_id.to_s,
                              :content => "You have become an official volunteer for #{task.name}")
          break
        end 
      end
    else 
      task.update_attributes(:current_volunteer_number => (task.current_volunteer_number - 1).to_s)
    end
    redirect_to task
  end
  
  private
    def register_params
      params.require(:register).permit(:task_id, :volunteer_id, :reg, :backup)
    end
end
