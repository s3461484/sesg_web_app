class VolunteersController < ApplicationController
  # Filters
  before_action :logged_in_volunteer, only: [:index, :show, :edit, :update]
  before_action :correct_volunteer,   only: [:edit, :update]
  before_action :admin_volunteer,     only: [:destroy, :appoint]
  before_action :check_volunteer,     only: [:show, :edit, :update, :appoint,
                                             :destroy, :reset, :score_board]
  
  # Show
  def index
    @filterrific = initialize_filterrific(
      Volunteer,
      params[:filterrific],
      :select_options => {
        sorted_by: Volunteer.options_for_sorted_by,
        core: Volunteer.options_for_core,
        with_tags: Volunteer.options_for_tags,
        with_programs: Volunteer.options_for_programs,
      }
    ) or return
    @volunteers = @filterrific.find.page(params[:page])

    respond_to do |format|
      format.html
      format.js
    end
    
  end
  
  def show
    @volunteer = Volunteer.find(params[:id])
    @events = Event.all
  end
  
  # Add
  def new
    @volunteer = Volunteer.new
  end

  def create
    local_params = volunteer_params.clone
    local_params[:points] = "0"
    if params[:volunteer][:image_link] == ""
      local_params[:image_link] = "http://www.vincegolangco.com/wp-content/uploads/2010/12/mickey-mouse-for-facebook.jpg".html_safe
    end
    @volunteer = Volunteer.new(local_params)
    if @volunteer.save
      log_in(@volunteer)
      redirect_to @volunteer
    else
      render 'new'
    end
  end
  
  # Edit
  def edit
    @volunteer = Volunteer.find(params[:id])
  end
  
  def update
    @volunteer = Volunteer.find(params[:id])
    if @volunteer.update_attributes(volunteer_params)
      flash[:success] = "Profile updated"
      redirect_to @volunteer
    else
      render 'edit'
    end
  end
  
  # Delete
  def destroy
    vol = Volunteer.find(params[:id])
    vol.destroy
    flash[:success] = "Volunteer deleted"
    AdminHistory.create(:content => "Deleted a volunteer: #{vol.name}")
    redirect_to volunteers_url
  end
  
  # Other functions
  def appoint
    local_params = {:core => true}
    @volunteer = Volunteer.find(params[:id])
    if !@volunteer.core? && !@volunteer.admin?
      @volunteer.update_attributes(local_params)
      AdminHistory.create(:content => "Appointed a core member: #{@volunteer.name}")
      Notification.create(:sender_id => "SESG", :receiver_id => @volunteer.id.to_s,
                          :content => "You have become a core team member!")
      flash[:success] = "Appointed #{@volunteer.name} to be core member."
      redirect_to(:back)
    else
      flash[:danger] = "You can't appoint a core member/admin"
      redirect_to '/volunteers'
    end
  end
  
  def reset
    local_params = {:points => 0}
    @volunteer = Volunteer.find(params[:id])
    if !@volunteer.core? && !@volunteer.admin?
      @volunteer.update_attributes(local_params)
      AdminHistory.create(:content => "Reset #{@volunteer.name}'s points".html_safe)
      Notification.create(:sender_id => "SESG", :receiver_id => @volunteer.id.to_s,
                          :content => "Your points have been reset.")
      flash[:success] = "#{@volunteer.name}'s points reset."
      redirect_to(:back)
    else
      flash[:danger] = "You can't reset that volunteer points"
      redirect_to '/volunteers'
    end
  end
  
  def score_board
    @volunteer = Volunteer.find(params[:id])
    @registers = @volunteer.registers
    @tasks = @volunteer.tasks
    @events = []
    @tasks.each do |t|
      if !@events.include?(t.event)
        @events.push(t.event)
      end 
    end
  end
  
  private

    def volunteer_params
      params.require(:volunteer).permit(:name, :email, :password,
                                   :password_confirmation, :image_link,
                                   :program, :phone)
    end
    
    # Before filters

    
    # Confirms the correct volunteer.
    def correct_volunteer
      @volunteer = Volunteer.find(params[:id])
      redirect_to(root_url) unless current_volunteer?(@volunteer)
    end
   
end
