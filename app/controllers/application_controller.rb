class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  include SesgPageHelper
  include TasksHelper
  include VolunteersHelper
  include EventsHelper
  before_filter :change_lang, :render_notis
  
  def change_lang
    if session[:locale].nil?
      session[:locale] = "en"
    end
    I18n.locale = session[:locale]
  end
  
  def render_notis
    if logged_in?
      notis = Notification.all
      @notis = []
      notis.each do |n|
        if n.created_at >= (Time.now - 7.days) && (n.receiver_id.include?(current_volunteer.id.to_s) || n.receiver_id == "all")
          @notis.push(n)
        end 
      end
    end
  end

  private
  
    def admin_volunteer
      redirect_to(root_url) unless current_volunteer.admin?
    end
    
    # Confirms a logged-in volunteer.
    def logged_in_volunteer
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
    def is_volunteer
      redirect_to(root_url) unless !current_volunteer.admin? && !current_volunteer.core?
    end
    
    def is_admin_or_core
      redirect_to(root_url) unless current_volunteer.admin? || current_volunteer.core?
    end

    def is_volunteer_or_core
      redirect_to(root_url) unless !current_volunteer.admin?
    end
end
