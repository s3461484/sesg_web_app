class PasswordResetsController < ApplicationController
  before_action :get_volunteer,   only: [:edit, :update]
  before_action :valid_volunteer, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]
  
  def new
  end

  def create
    @volunteer = Volunteer.find_by(email: params[:password_reset][:email].downcase)
    if @volunteer
      @volunteer.create_reset_digest
      @volunteer.send_password_reset_email
      flash[:info] = "Email sent with password reset instructions"
      redirect_to root_url
    else
      flash.now[:danger] = "Email address not found"
      render 'new'
    end
  end

  def edit
  end
  
  def update
    if params[:volunteer][:password].empty?
      flash.now[:danger] = "Password can't be empty"
      render 'edit'
    elsif @volunteer.update_attributes(volunteer_params)
      @volunteer.update_attributes(:reset_digest => nil, :reset_sent_at => nil)
      log_in @volunteer
      flash[:success] = "Password has been reset."
      redirect_to @volunteer
    else
      render 'edit'
    end
  end
  
  private
    def volunteer_params
      params.require(:volunteer).permit(:password, :password_confirmation)
    end
    
    def get_volunteer
      @volunteer = Volunteer.find_by(email: params[:email])
    end

    # Confirms a valid volunteer.
    def valid_volunteer
      unless (@volunteer && (@volunteer.authenticated?("reset", params[:id])))
        redirect_to root_url
      end
    end
    
    def check_expiration
      if @volunteer.password_reset_expired?
        flash[:danger] = "Password reset has expired."
        redirect_to new_password_reset_url
      end
    end
end