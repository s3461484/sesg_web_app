class VolunteerMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.volunteer_mailer.account_activation.subject
  #
  def account_activation(vol)
    @volunteer = vol
    mail to: vol.email, subject: "Account Activation"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.volunteer_mailer.password_reset.subject
  #
  def reminder(vol, task)
    @task = task
    @event = task.event
    @volunteer = vol
    mail to: vol.email, subject: "Task Reminder"
  end
  
  def password_reset(vol)
    @volunteer = vol
    mail to: vol.email, subject: "Password reset"
  end
end
