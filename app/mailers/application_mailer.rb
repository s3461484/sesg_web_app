class ApplicationMailer < ActionMailer::Base
  default from: "sesg.sgs@rmit.edu.vn"
  layout 'mailer'
end