Rails.application.routes.draw do
  root 'sesg_page#home'
  
  resources :volunteers
  resources :events
  resources :tasks , only: [:create, :edit, :update, :destroy, :show]
  resources :registers, only: [:create, :destroy]
  resources :notifications, only: [:create, :destroy]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  
  get     'about'                   => 'sesg_page#about'
  get     'contact'                 => 'sesg_page#contact'
  get     'signup'                  => 'volunteers#new'
  get     'login'                   => 'sessions#new'
  get     'tasks/new/:event_id'     => 'tasks#new'
  get     'help'                    => 'sesg_page#help'
  get     'score_board/:id'         => 'volunteers#score_board'
  get     'admin_history'           => 'admin_histories#new'
  get     'search'                  => 'volunteers#search'
  get     'change_locale/:lang'     => 'sessions#change_locale'
  
  
  post    'reset/volunteers/:id'    => 'volunteers#reset'
  post    'assess'                  => 'registers#update_all'
  post    'appoint/volunteers/:id'  => 'volunteers#appoint'
  post    'login'                   => 'sessions#create'
  
  delete  'logout'                  => 'sessions#destroy'
end