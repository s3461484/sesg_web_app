# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150804084514) do

  create_table "admin_histories", force: :cascade do |t|
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "image_link"
  end

  create_table "notifications", force: :cascade do |t|
    t.string   "sender_id"
    t.string   "receiver_id"
    t.text     "event_id"
    t.text     "content"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "registers", force: :cascade do |t|
    t.integer  "volunteer_id"
    t.integer  "task_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "tag"
    t.boolean  "backup",       default: false
  end

  add_index "registers", ["task_id"], name: "index_registers_on_task_id"
  add_index "registers", ["volunteer_id"], name: "index_registers_on_volunteer_id"

  create_table "restrictions", force: :cascade do |t|
    t.integer  "task_id"
    t.string   "tag"
    t.string   "task_restrict"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "restrictions", ["task_id"], name: "index_restrictions_on_task_id"

  create_table "tasks", force: :cascade do |t|
    t.integer  "event_id"
    t.string   "name"
    t.text     "description"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "awn"
    t.integer  "awon"
    t.integer  "lwn"
    t.integer  "lwon"
    t.integer  "ontime"
    t.integer  "bonus"
    t.integer  "minus"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.boolean  "task_type"
    t.integer  "max_volunteer_number"
    t.integer  "current_volunteer_number"
    t.integer  "max_backup"
    t.integer  "current_backup"
  end

  add_index "tasks", ["event_id"], name: "index_tasks_on_event_id"

  create_table "volunteers", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.integer  "points"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",           default: false
    t.boolean  "core",            default: false
    t.string   "program"
    t.string   "image_link"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_index "volunteers", ["email"], name: "index_volunteers_on_email", unique: true

end
