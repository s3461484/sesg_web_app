class RemoveActivationFromVolunteers < ActiveRecord::Migration
  def change
    remove_column :volunteers, :activation_digest, :string
    remove_column :volunteers, :activated, :boolean
    remove_column :volunteers, :activated_at, :datetime
  end
end
