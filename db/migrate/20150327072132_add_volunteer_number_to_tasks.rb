class AddVolunteerNumberToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :max_volunteer_number, :integer
    add_column :tasks, :current_volunteer_number, :integer
  end
end
