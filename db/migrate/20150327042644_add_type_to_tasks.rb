class AddTypeToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :type, :boolean
  end
end
