class AddTagToRegister < ActiveRecord::Migration
  def change
    add_column :registers, :tag, :string
  end
end
