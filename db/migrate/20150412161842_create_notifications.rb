class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :sender_id
      t.string :receiver_id
      t.text :event_id
      t.text :content

      t.timestamps null: false
    end
  end
end
