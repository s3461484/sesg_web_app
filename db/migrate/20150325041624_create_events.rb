class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.datetime :start_date
      t.datetime :end_date
      t.integer :max_volunteer_number
      t.integer :current_volunteer, default: 0

      t.timestamps null: false
    end
  end
end
