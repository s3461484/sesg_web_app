class CreateRestrictions < ActiveRecord::Migration
  def change
    create_table :restrictions do |t|
      t.references :task, index: true
      t.string :tag
      t.string :task_restrict

      t.timestamps null: false
    end
    add_foreign_key :restrictions, :tasks
  end
end
