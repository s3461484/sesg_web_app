class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :event, index: true
      t.string :name
      t.text :description
      t.datetime :start_time
      t.datetime :end_time
      t.integer :awn
      t.integer :awon
      t.integer :lwn
      t.integer :lwon
      t.integer :ontime
      t.integer :bonus
      t.integer :minus

      t.timestamps null: false
    end
    add_foreign_key :tasks, :events
  end
end
