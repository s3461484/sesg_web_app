class AddBackupToRegister < ActiveRecord::Migration
  def change
    add_column :registers, :backup, :boolean, default: false
  end
end