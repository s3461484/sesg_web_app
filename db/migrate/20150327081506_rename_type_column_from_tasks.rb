class RenameTypeColumnFromTasks < ActiveRecord::Migration
  def change
    change_table :tasks do |t|
      t.rename :type, :task_type
    end
  end
end
