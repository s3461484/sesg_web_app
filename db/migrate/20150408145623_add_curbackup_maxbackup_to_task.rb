class AddCurbackupMaxbackupToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :max_backup, :integer
    add_column :tasks, :current_backup, :integer
  end
end
