class AddProgramToVolunteers < ActiveRecord::Migration
  def change
    add_column :volunteers, :program, :string
  end
end
