class AddCoreToVolunteers < ActiveRecord::Migration
  def change
    add_column :volunteers, :core, :boolean, default: false
  end
end
