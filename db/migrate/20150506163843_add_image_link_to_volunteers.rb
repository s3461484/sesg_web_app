class AddImageLinkToVolunteers < ActiveRecord::Migration
  def change
    remove_attachment :volunteers, :avatar
    add_column :volunteers, :image_link, :string
  end
end
