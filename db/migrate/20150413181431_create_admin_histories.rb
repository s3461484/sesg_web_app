class CreateAdminHistories < ActiveRecord::Migration
  def change
    create_table :admin_histories do |t|
      t.text :content

      t.timestamps null: false
    end
  end
end
