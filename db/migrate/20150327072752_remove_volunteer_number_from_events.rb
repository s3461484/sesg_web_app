class RemoveVolunteerNumberFromEvents < ActiveRecord::Migration
  def change
    remove_column :events, :max_volunteer_number, :integer
    remove_column :events, :current_volunteer, :integer
  end
end
