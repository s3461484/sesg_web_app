class CreateRegisters < ActiveRecord::Migration
  def change
    create_table :registers do |t|
      t.references :volunteer, index: true
      t.references :task, index: true

      t.timestamps null: false
    end
    add_foreign_key :registers, :volunteers
    add_foreign_key :registers, :tasks
  end
end
