require 'test_helper'

class VolunteerMailerTest < ActionMailer::TestCase

  test "account_activation" do
    volunteer = volunteers(:michael)
    volunteer.activation_token = Volunteer.new_token
    mail = VolunteerMailer.account_activation(volunteer)
    assert_equal "Account Activation", mail.subject
    assert_equal [volunteer.email], mail.to
    assert_equal ["sesg.sgs@rmit.edu.vn"], mail.from
    assert_match volunteer.name,               mail.body.encoded
    assert_match volunteer.activation_token,   mail.body.encoded
    assert_match CGI::escape(volunteer.email), mail.body.encoded
  end

  test "password_reset" do
    mail = VolunteerMailer.password_reset
    assert_equal "Password reset", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end
end