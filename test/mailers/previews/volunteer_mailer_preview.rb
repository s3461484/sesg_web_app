# Preview all emails at http://localhost:3000/rails/mailers/volunteer_mailer
class VolunteerMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/volunteer_mailer/account_activation
  def account_activation
    volunteer = Volunteer.first
    volunteer.activation_token = Volunteer.new_token
    VolunteerMailer.account_activation(volunteer)
  end

  # Preview this email at http://localhost:3000/rails/mailers/volunteer_mailer/password_reset
  def password_reset
    VolunteerMailer.password_reset
  end

end
