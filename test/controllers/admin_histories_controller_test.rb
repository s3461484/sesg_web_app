require 'test_helper'

class AdminHistoriesControllerTest < ActionController::TestCase
  
  def setup
    @admin = volunteers(:michael)
    log_in(@admin)
  end
  
  test "should get new" do
    get :new
    assert_response :success
  end
  
end
