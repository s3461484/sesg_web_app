require 'test_helper'
include SessionsHelper

class EventsControllerTest < ActionController::TestCase
  
  def setup
    @event = events(:one)
    @vol = volunteers(:michael)
    log_in(@vol)
  end
  
  test "should get index" do
    get :index
    assert_response :success
    assert_template :index
  end
  
  test "should get show" do
    get :show, id: @event.id
    assert_response :success
    assert_template :show
  end
  
  test "should get new" do
    get :new
    assert_response :success
    assert_template :new
  end
  
  test "should get edit" do
    get :edit, id: @event.id
    assert_response :success
    assert_template :edit
  end
  
  test "can update information" do
    log_in(@vol)
    patch :update, id: @event.id, event: {name: "lalala", description: "lololo"}
    assert_response :found
    assert_equal "lalala", Event.find(@event.id).name
    assert_equal "lololo", Event.find(@event.id).description
  end
  
  test "can create" do
    log_in(@vol)
    num_evt = Event.all.size
    post :create, event: {"name"=> "hohoho", "decription"=> "hahaha", 
      "start_date(1i)"=> "1", "start_date(2i)"=> "2", "start_date(3i)"=> "3",
      "end_date(1i)"=> "2", "end_date(2i)"=> "3", "end_date(3i)"=> "4"
    }
    assert_response :found
    assert_not_nil Event.find_by(name: "hohoho")
    assert_equal Event.all.size, num_evt + 1
  end
  
  test "can delete" do
    log_in(@vol)
    num_evt = Event.all.size
    delete :destroy, id: @event.id
    assert_equal num_evt - 1, Event.all.size
  end
end
