require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  
  def setup
    @vol = volunteers(:michael)
    request.env['HTTP_REFERER'] = '/'
  end

  test "should get new" do
    get :new
    assert_response :success
  end
  
  test "can login" do
    post :create, session: {email: "michael@rmit.edu.vn", password: "password"}
    assert_equal @vol.id, current_volunteer.id
  end
  
  test "can change language" do
    get :change_locale, lang: "vn"
    assert_equal "vn", session[:locale] 
    get :change_locale, lang: "en"
    assert_equal "en", session[:locale]
  end
  
  test "can logout" do
    log_in(@vol)
    delete :destroy, id: @vol.id
    assert_equal nil, current_volunteer
  end

end
