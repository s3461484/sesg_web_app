require 'test_helper'
include SessionsHelper

class VolunteersControllerTest < ActionController::TestCase
  
  def setup
    @vol = volunteers(:michael)
    log_in(@vol)
    request.env['HTTP_REFERER'] = '/volunteers'
  end
  
  test "should get new" do
    get :new
    assert_response :success
    assert_template :new
  end
  
  test "should get index" do
    get :index
    assert_response :success
    assert_template :index
  end
  
  test "should get show" do
    get :show, id: @vol.id
    assert_response :success
    assert_template :show
  end
  
  test "should get score_board" do
    get :score_board, id: @vol.id
    assert_response :success
    assert_template :score_board
  end
  
  test "should get edit" do
    get :edit, id: @vol.id
    assert_response :success
    assert_template :edit
  end
  
  test "can update information" do
    log_in(@vol)
    patch :update, id: @vol.id, volunteer: {name: "chau xinh", email:"s3461484@rmit.edu.vn",
    phone: "1234563330", password: "password", password_confirmation:"password"}
    assert_response :success
  end
  
  test "can sign up" do
    log_out
    num_vol = Volunteer.all.size
    post :create, volunteer: {name: "chau khung", email:"s3461484@rmit.edu.vn",
    phone: "0123456789", password: "password", password_confirmation:"password"}
    assert_response :found
    assert_not_nil Volunteer.find_by(name: "chau khung")
    assert_equal Volunteer.all.size, num_vol + 1
    assert_equal Volunteer.find_by(name: "chau khung").activated, false
  end
  
  test "can appoint core team" do
    log_in(@vol)
    post :appoint, id: Volunteer.find_by(name: "Sterling Archer").id
    assert_response :found
    assert_redirected_to "/volunteers"
  end
  
  test "can reset point" do
    log_in(@vol)
    post :reset, id: Volunteer.find_by(name: "Sterling Archer").id
    assert_response :found
    assert_redirected_to "/volunteers"
  end
  
  test "can delete" do
    log_in(@vol)
    delete :destroy, id: Volunteer.find_by(name: "Sterling Archer").id
    assert_nil Volunteer.find_by(name: "Sterling Archer")
    assert_response :found
    assert_redirected_to "/volunteers"
  end
end
