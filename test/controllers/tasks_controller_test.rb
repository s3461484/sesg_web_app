require 'test_helper'

class TasksControllerTest < ActionController::TestCase
  
  def setup
    @event = events(:one)
    @vol = volunteers(:michael)
    @task = tasks(:one)
    log_in(@vol)
  end
  
  test "should get show" do
    get :show, id: @task.id
    assert_response :success
    assert_template :show
  end
  
  test "should get new" do
    get :new, event_id: @event.id 
    assert_response :success
    assert_template :new
  end
  
  test "should get edit" do
    get :edit, id: @task.id
    assert_response :success
    assert_template :edit
  end
  
  test "can update information" do
    log_in(@vol)
    patch :update, id: @task.id, task: {"name"=> "lalala", "description"=> "hahaha", 
    "start_time(1i)"=> "1", "start_time(2i)"=> "2", "start_time(3i)"=> "3", "start_time(4i)" => "4", "start_time(5i)" => "5",
      "end_time(1i)"=> "2", "end_time(2i)"=> "3", "end_time(3i)"=> "4", "end_time(4i)" => "4", "end_time(5i)" => "5",
      awn: -2, awon: -3, lwn: -1, lwon: -2, ontime: 2, bonus: 1, minus: -1}
    assert_response :found
    assert_equal "lalala", Task.find(@task.id).name
    assert_equal "hahaha", Task.find(@task.id).description
    assert_equal -3, Task.find(@task.id).awon
  end
  
  test "can create" do
    log_in(@vol)
    num_task = Task.all.size
    post :create, id: @event.id, task: {"event_id" => @event.id, "name"=> "hohoho", "decription"=> "hahaha", 
      "start_time(1i)"=> "1", "start_time(2i)"=> "2", "start_time(3i)"=> "3", "start_time(4i)" => "4", "start_time(5i)" => "5",
      "end_time(1i)"=> "2", "end_time(2i)"=> "3", "end_time(3i)"=> "4", "end_time(4i)" => "4", "end_time(5i)" => "5",
      awn: -2, awon: -3, lwn: -1, lwon: -2, ontime: 2, bonus: 1, minus: -1,
      max_volunteer_number: 3, max_backup: 4,
      "restrictions" => {"restriction_task" => "", "restriction_tag" => ""}
    }
    assert_response :found
    assert_not_nil Task.find_by(name: "hohoho")
    assert_equal Task.all.size, num_task + 1
  end
  
  test "can delete" do
    log_in(@vol)
    num_task = Task.all.size
    delete :destroy, id: @task.id
    assert_equal num_task - 1, Task.all.size
  end
  
end
