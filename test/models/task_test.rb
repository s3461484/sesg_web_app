require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  
  def setup
    @task = Task.new(name: "Play dota", start_time: DateTime.now, 
                     end_time: DateTime.now + 1.days, awn: 1, awon: 1, lwn: 1,
                     lwon: 1, ontime:1 , bonus: 1, minus: 1, 
                     max_volunteer_number: 1)
  end
  
  test "Task name must be present" do
    @task.name = ""
    assert_not @task.valid?
  end
  
  test "Task name must not be too long" do
    @task.name = "a" * 51
    assert_not @task.valid?
  end
  
  test "Task start time must be present" do
    @task.start_time = nil
    assert_not @task.valid?
  end
  
  test "End time must be present" do
    @task.end_time = nil
    assert_not @task.valid?
  end
  
  test "awn point must be present" do
    @task.awn = nil
    assert_not @task.valid?
  end
  
  test "awon point must be present" do
    @task.awon = nil
    assert_not @task.valid?
  end
  
  test "lwn point must be present" do
    @task.lwn = nil
    assert_not @task.valid?
  end
  
  test "lwon point must be present" do
    @task.lwon = nil
    assert_not @task.valid?
  end
  
  test "ontime point must be present" do
    @task.ontime = nil
    assert_not @task.valid?
  end
  
  test "bonus point must be present" do
    @task.bonus = nil
    assert_not @task.valid?
  end
  
  test "minus point must be present" do
    @task.minus = nil
    assert_not @task.valid?
  end
  
  test "Max volunteer number must be present" do
    @task.max_volunteer_number = nil
    assert_not @task.valid?
  end
  
  test "End time should be after start time" do
    @task.end_time = Time.now
    @task.start_time = Time.now + 10.days
    assert_not @task.valid?
  end
end
