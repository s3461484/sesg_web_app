require 'test_helper'

class VolunteerTest < ActiveSupport::TestCase
  
  def setup
    @user = Volunteer.new(name: "Chau xinh", phone: "0909123456", email: "abc@rmit.edu.vn", 
    points: 20, program: "Bachelor of IT", password: "aaaaaaa")
  end
  
  test "Should be RMIT VN email account" do
    @user.email = "example-120@rmit.edu.vn"
    assert @user.valid?
  end
  
  test "Invalid email" do
    @user.email = "abc@gmail.com"
    assert_not @user.valid?
  end
  
  test "name should be present" do
    @user.name = ""
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end
  
  test "email should not be too long" do
    @user.name = "a" * 256 + "@rmit.edu.vn"
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end
  
  test "Invalid password" do
    @user.password = "a" * 5
    assert_not @user.valid?
  end
  
  test "Password must be at least 6 characters" do
    @user.password = "b" * 6
    assert @user.valid?
  end
  
  test "Points must be integer" do
    @user.points = "b"
    assert_not @user.valid?
  end
end
