require 'test_helper'

class EventTest < ActiveSupport::TestCase
  
  def setup
    @event = Event.new(name: "whatever", start_date: Time.now, end_date: Time.now + 1.days)
  end
  
  test "Event name should be present" do
    @event.name = ""
    assert_not @event.valid?
  end
  
  test "Event name should not be too long" do
    @event.name = "a" * 51
    assert_not @event.valid?
  end
   
  test "Start date should be present" do
    @event.start_date = nil
    assert_not @event.valid?
  end
  
  test "End date should be present" do
    @event.end_date = nil
    assert_not @event.valid?
  end
  
  test "End date should be after start date" do
    @event.end_date = Time.now
    @event.start_date = Time.now + 10.days
    assert_not @event.valid?
  end
  
end
